<?php
/**
 * @file
 * Views hooks for the signage_panes module.
 */

/**
 * Implements hook_views_post_build().
 */
function signage_panes_views_post_build($view) {
  if ($view->name == 'signage_rss_feeds') {
    $feed_url = $view->display_handler->options['pane_conf']['feed_url'];
    $view->query->options['xml_file'] = $feed_url;
    $description = $view->display_handler->options['pane_conf']['show_description'];
    if ($description) {
      $markup = '<span class="published-date">[date]</span><span class="text">[text] - [text_1]</span>';
      $view->query->fields['title']['alter']['text'] = $markup;
    }
  }
}
