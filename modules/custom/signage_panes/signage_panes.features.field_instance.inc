<?php
/**
 * @file
 * signage_panes.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function signage_panes_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'fieldable_panels_pane-image_pane-field_pane_image'
  $field_instances['fieldable_panels_pane-image_pane-field_pane_image'] = array(
    'bundle' => 'image_pane',
    'deleted' => 0,
    'description' => 'Upload an image to display. We recommend you do any image manipulation before uploading. The image will be displayed the same size as uploaded, scaled to the width of the image\'s container.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_pane_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'panes',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Image');
  t('Upload an image to display. We recommend you do any image manipulation before uploading. The image will be displayed the same size as uploaded, scaled to the width of the image\'s container.');

  return $field_instances;
}
