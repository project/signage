; signage make file for development
core = "7.x"
api = "2"

projects[drupal][version] = "7.28"

; Download signage profile.

projects[signage][type] = profile
projects[signage][download][type] = git
projects[signage][download][url] = "http://git.drupal.org/project/signage.git"
projects[signage][download][branch] = 7.x-1.x



